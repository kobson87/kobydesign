<!doctype html>

<html lang="en">
<head>

  <meta charset="utf-8">
  <title><?php wp_title( '-', true, 'right' ); ?></title>
  <meta name="description" content="">
  <meta name="keywords" content="clean, simplicity, theme, html5, sidebar, masonry">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="author" content="kobydesign.pl">

<!--Style-->

  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/reset.css">
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style.css">
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/style-responsive.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
 
  <script src="<?php bloginfo('template_url'); ?>/assets/js/modernizr.custom.42534.js" type="text/javascript"></script>

  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  
</head>

<?php wp_head(); ?>

<body>

  <!--Preloader-->

<div class="preloader" id="preloader">
    <div class="item">
      <div class="spinner">
      </div>
    </div>
</div>

<!--Header-->
<header class="boxed" id="header-white">

  <div class="header-margin">

    <a class="logo" href="<?php bloginfo('wpurl'); ?>"><?php get_template_part('assets/img/kobydesign-logotyp'); ?></a>
    <!--<div class="menu-index" id="button"><i class="fa fa-bars"></i></div>  -->
    
  </div>

</header>

<div class="clear"></div>
