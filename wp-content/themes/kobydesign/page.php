<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<?php
$sidebar = '';
if(is_page('nasi-specjalisci'))
{
	$sidebar = 'specialist';
}
?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="container">
	<section>
		<header class="section-header">
			<h1 class="section-title"><?php the_title(); ?></h1>
		</header>
		<div class="row">
			
			<article class="col-md-7 col-md-push-5">
				
				<?php the_content(); ?>

			</article>
			
			<?php get_template_part('elements/sidebar', $sidebar); ?>

		</div>
	</section>
</div>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>