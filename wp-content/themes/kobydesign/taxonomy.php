<?php
$queried_object = get_queried_object();
$tax_name = $queried_object->name;
?>

<?php get_header(); ?>

<div class="section banner-deep item garden-bg"></div>

<div class="container">
	<div class="section-frame">
		<div class="row">
			<div class="col-lg-12">
				<a class="back-button button left" href="<?php echo wp_get_referer(); ?>">
					<span class=""><span class="icon icon-arrow-left"></span>powrót</span>
				</a>	
				<h1 class="title green text-center"><span class="underline"><?php echo $tax_name; ?></span></h1>
			</div>
		</div>

		<?php get_template_part('elements/loop'); ?>

	</div>
</div>

<?php get_footer(); ?>