  <!--Footer-->

  <footer>

    <div class="footer-margin">
      <div class="social-footer">
        <a href="https://www.facebook.com/kobydesign"><i class="fa fa-facebook"></i></a>
        <a href="https://www.behance.net/kobydesign"><i class="fa fa-behance"></i></a>
      </div>     
      <div class="copyright">© Copyright 2017 Kobydesign.pl. All rights reserved.</div> 
    </div>


  </footer>

<?php wp_footer(); ?>

<!--Scripts-->

  <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.min.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.easing.min.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.waitforimages.js" type="text/javascript"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/typed.js" type="text/javascript"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/imagesloaded.pkgd.min.js" type="text/javascript"></script>    
  <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.jkit.1.2.16.min.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/script.js" type="text/javascript"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.nicescroll.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.parallax.js"></script> <!-- jQuery Parallax -->   
  <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.hc-sticky.min.js"></script>  
  <script src="<?php bloginfo('template_url'); ?>/assets/js/masonry.pkgd.min.js" type="text/javascript"></script>  
  <script>
    $('#button').on('click', function() {
      $( ".opacity-nav" ).fadeToggle( "slow", "linear" );
    // Animation complete.
    });
  </script>


</body>
</html>