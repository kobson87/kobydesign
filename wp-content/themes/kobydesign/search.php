<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

<?php get_header(); ?>


<div class="container">
	<section>
		<header class="section-header">
			<h1 class="section-title">Szukaj</h1>
		</header>
		<div class="row">
			
			<article class="col-md-7 col-md-push-5">

				<h1>Wyniki wyszukiwania dla frazy: <?php echo trim( get_search_query() ); ?></h1>
				
				<?php if ( have_posts() ): ?>

				<div class="search-list">

					<?php while ( have_posts() ): the_post(); ?>

					<?php get_template_part('list_item', 'search'); ?>

					<?php endwhile; wp_reset_postdata(); ?>

				</div>

				<div class="clearfix">
					<?php get_template_part('pagination'); ?>
				</div>

				<?php else: ?>

				<?php get_template_part('content', 'none'); ?>

				<?php endif; ?>

			</article>
			
			<?php get_template_part('elements/sidebar'); ?>
			
		</div>
	</section>
</div>

<?php get_footer(); ?>