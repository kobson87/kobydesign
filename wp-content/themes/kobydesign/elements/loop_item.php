<a href="<?php the_permalink(); ?>" target="blank" class="ajax-link">
	<div class="grid-hover">
	   	<h1><?php echo get_the_title(); ?></h1>
        <p><?php the_tags( '', '&nbsp&nbsp|&nbsp&nbsp', '' ); ?></p>
    </div>
    <?php if( has_post_thumbnail() ): ?>
		<?php the_post_thumbnail('post-thumbnail', array('class' => '') ); ?>
	<?php endif; ?>
 </a>  