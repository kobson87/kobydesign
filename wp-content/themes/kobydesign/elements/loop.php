<?php if( have_posts() ): ?>

<div class="row">

	<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part('elements/loop_item'); ?>

	<?php endwhile; ?>

</div>

<?php else: ?>

	<?php get_template_part('content', 'none'); ?>

<?php endif; ?>

<?php get_template_part('pagination'); ?>