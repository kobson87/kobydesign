<?php //template name: Strona główna ?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

  <!--Content-->

<div class="content" id="ajax-content">

  <div class="container">
        <div class="text-intro" id="site-type">

          <h1>I am Koby. I design.</h1>
          <h1>This is my <span class="typewrite">Portfolio</span></h1>
          <p>"Man can believe the impossible, but man can never believe the improbable." Oscar Wilde</p>

        </div>

      <ul class="portfolio-grid">
        <?php
        // Query vars
        $paged = 1;
        if ( get_query_var('paged') ) $paged = get_query_var('paged');
        if ( get_query_var('page') ) $paged = get_query_var('page');

        // Args
        $args = [
          'post_type' => 'post',
          'orderby' => 'date',
          'paged' => $paged,
        ];


        // Loop
        $loop = new WP_Query($args);
        ?>

        <?php if( $loop->have_posts() ): ?>
            <?php while($loop->have_posts()): $loop->the_post(); ?>
            
        <li class="grid-item" data-jkit="[show:delay=3000;speed=500;animation=fade]">
            <?php get_template_part('elements/loop_item'); ?>
        </li>
            <?php endwhile; ?>

          <?php wp_reset_postdata(); ?>

        <?php else: ?>
          <?php echo "none" ?>
        <?php endif; ?>

      </ul>
      
      <div class="clear section"></div>

  </div>

  <!--About me-->

  <div class="container">
      
        <div class="text-intro">

          <h1>About me</h1>
                    
          <div class="one-column">
            <p>Brand designer | Product Designer | Web Junior Designer | Traveler</p>
          
          </div>


          <div class="two-column">

            <p>Hello, my name is Michał Kobierny. I'm graphic designer. I'm freelancer. I invite you to enter my world, world of design.<br/><br/>

            I love branding, meeting with new client, discussing about ideas. I like product design, good looking ads. I learn web design, UX design, mobile design. I code html/php wordpress templates. All the time going forward.
            
            <br/><br/>
            I encourage you to watch my portfolio. Do you like it? Let's do something together!
            
            </p>
          
          </div>          


        </div>
        
        <div class="clear"></div>

  </div>
  
  <!--Contact-->

    <div class="container">

          <div class="text-intro">

          <h1>Contact</h1>
          <p>To contact me please use the contact form below</p>

          <?php the_content(); ?>

          <!--<form method="post" action="?" data-jkit="[form:validateonly=true]">
          
            <div class="contact-one">
              <p>
                <label for="miniusername">Name:</label><br />
                <input name="miniusername" id="miniusername" data-jkit="[validate:required=true;min=3;max=10;error=Please enter your username (3-10 characters)]">
              </p>
            </div>  
            
            <div class="contact-two">  
              <p>
                <label for="miniemail">E-mail:</label><br />
                <input name="miniemail" id="miniemail" data-jkit="[validate:required=true;strength=50;error=Please write your email]">
              </p>
            </div>  
            
            <div class="contact-three">  
              <p>
                <label>Message:</label><br />
                <textarea id="message" name="message"></textarea><br/><br/>
                <input class="button-submit" name="send" type="submit" value="SUBMIT" />
              </p>
            </div>            
              
              <p>
              </p>
          </form>
          
        </div>-->

        <div class="clear"></div>

    </div>

</div>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>