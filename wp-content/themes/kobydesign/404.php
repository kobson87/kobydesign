<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<div class="container">
	<section>
		<header class="section-header">
			<h1 class="section-title"><?php _e('Error'); ?> 404</h1>
		</header>
		<div class="row">
			
			<article class="col-xxs-12">
				
				<h3><?php _e('Error'); ?>!</h3>
				<p><?php _e('Wybrana strona nie istnieje.', 'doktorogrodnik'); ?></p>
				<p><?php printf( __( 'Przejdź do <a href="%s"><span>strony głównej</span>.</a>', 'doktorogrodnik' ), get_bloginfo( 'url' ) ); ?></p>

			</article>
		</div>
	</section>
</div>

	
<?php get_footer(); ?>