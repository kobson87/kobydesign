<?php

/*-----------------------------------------------------------------------------
	Theme setup - based on Twenty Twelve theme
	-------------------------------------------------------------------------*/

/**
 * Sets up theme defaults and registers the various WordPress features that
 * Twenty Twelve supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, aukobydesigntic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Twelve 1.0
 */
function kobydesign_setup() {
	/*
	 * Makes Twenty Twelve available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Twelve, use a find and replace
	 * to change 'kobydesign' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'kobydesign', get_template_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'kobydesign_setup' );


/**
 * Enqueues scripts and styles for front-end.
 *
 * @since Twenty Twelve 1.0
 */
function kobydesign_scripts_styles() {
	global $wp_styles;

	/*
	 * Loads our main stylesheet.
	 */
	wp_enqueue_style( 'kobydesign-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'kobydesign_scripts_styles' );

/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @since Twenty Twelve 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string Filtered title.
 */
function kobydesign_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'kobydesign' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'kobydesign_wp_title', 10, 2 );

/**
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
 *
 * @since Twenty Twelve 1.0
 */
function kobydesign_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'kobydesign_page_menu_args' );


/*-----------------------------------------------------------------------------
	Utility functions
	-------------------------------------------------------------------------*/

// format_link
function format_link($link)
{
	return strpos($link, 'https://') !== FALSE ?  $link : 'http://'.str_replace('http://', '', $link);
}

// get_page_template_by_ID
function get_page_template_by_ID($id)
{
	return get_post_meta( $id, '_wp_page_template', true );
}

// get_page_link_by_path
function get_page_link_by_path($path)
{
	$page = get_page_by_path($path);
	return $page->ID ? get_permalink($page->ID) : "";
}

// get_ID_by_slug
function get_ID_by_slug($page_slug)
{
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

// get_page_children_all
function get_page_children_all($pageID)
{
	// Set up the objects needed
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));

	return get_page_children( $pageID, $all_wp_pages );
}

// get_fa_from_mime
function get_fa_from_mime($mime)
{
	$fa_icons = array(
		'application/pdf' => 'fa-file-pdf-o',
		'application/msword' => 'fa-file-word-o',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'fa-file-word-o',
		'application/x-compressed' => 'fa-file-zip-o',
		'application/zip' => 'fa-file-zip-o',
		'application/x-zip-compressed' => 'fa-file-zip-o',
		'video/mp4' => 'fa-file-movie-o',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'fa-file-excel-o',
		'application/excel' => 'fa-file-excel-o',
		'application/vnd.ms-excel' => 'fa-file-excel-o',
		'application/x-excel' => 'fa-file-excel-o',
		'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'fa-file-powerpoint-o',
		'application/powerpoint' => 'fa-file-powerpoint-o',
		'image/jpeg' => 'fa-file-photo-o',
		'image/gif' => 'fa-file-photo-o',
		'image/png' => 'fa-file-photo-o',
		'text/plain' => 'fa-file-text-o',
		'default' => 'fa-file-o',
	);

	if(isset($fa_icons[$mime]))
	{
		$fa = $fa_icons[$mime];
	}
	else
	{
		$fa = $fa_icons['default'];
	}

	return $fa;
}

// the_slug
function the_slug($echo=true)
{
	if(is_404())
	{
		$slug = __('Error');
	}
	else
	{
		$slug = basename(get_permalink());
	}
	do_action('before_slug', $slug);
	$slug = apply_filters('slug_filter', $slug);
	if( $echo ) echo $slug;
	do_action('after_slug', $slug);
	return $slug;
}

// get_the_slug
function get_the_slug()
{
	return the_slug($echo=FALSE);
}

// get_current_user_name
function get_current_user_name()
{
	global $current_user;
	$current_user = wp_get_current_user();
	return $current_user->user_login;
}

// Check if page is an ancestor
function is_ancestor($post_id)
{
    global $wp_query;
    $ancestors = $wp_query->post->ancestors;
    if ( in_array($post_id, $ancestors) ) {
        return true;
    } else {
        return false;
    }
}