<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="content" id="ajax-content">

	<div class="container">

        <div class="text-intro">

          <h1><?php echo get_the_title(); ?></h1>
          
	          <div class="one-column">
	            <p><?php the_tags( '', '&nbsp&nbsp|&nbsp&nbsp ', '' ); ?></p>
	          </div>


		          <div class="two-column">
		            <p><?php echo the_content(); ?></p>
		          </div>   
                 				  
                <div class="clear"></div>   				  

			        <div class="prev-next">
				        
			          <div class="prev-button"><a class="ajax-link" href="<?php echo previous_posts_link(); ?>">Previous work</a></div>
			          <div class="next-button"><a class="ajax-link" href="<?php echo next_posts_link(); ?>">Next work</a></div>
					
			        </div>
         </div>
	</div>
</div>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>