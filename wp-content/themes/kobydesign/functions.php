<?php

include_once 'inc/base-functions.php';

/*-----------------------------------------------------------------------------
	Optional theme configuration
	-------------------------------------------------------------------------*/

// Theme setup
function kobydesign_setup_custom()
{
	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'top-menu', __( 'Top Menu', 'kobydesign' ) );

	add_image_size('post-thumbnail', 420, 9999, FALSE);
	//add_image_size('post-img-basic', 300, 300, TRUE);
	//add_image_size('post-img-full', 1000, 9999, FALSE);
	//add_image_size('post-img-small', 330, 220, TRUE);
	//add_image_size('post-img-full', 1000, 9999, FALSE);
	//add_image_size('news-thumb', 246, 105, TRUE);

}
add_action( 'after_setup_theme', 'kobydesign_setup_custom' );

/**
 * Registers our main widget area and the front page widget areas.
 *
 * @since Twenty Twelve 1.0
 */
function kobydesign_widgets_init() {
	// register_sidebar( array(
	// 	'name' => __( 'Stopka', 'kobydesign' ),
	// 	'id' => 'sidebar-1',
	// 	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	// 	'after_widget' => '</div>',
	// 	'before_title' => '<h3 class="widget-title">',
	// 	'after_title' => '</h3>',
	// ) );
}
add_action( 'widgets_init', 'kobydesign_widgets_init' );

/**
 * Extends the default WordPress body class to denote:
 * 1. Using a full-width layout, when no active widgets in the sidebar
 *    or full-width template.
 * 2. Front Page template: thumbnail in use and number of sidebars for
 *    widget areas.
 * 3. White or empty background color to change the layout and spacing.
 * 4. Custom fonts enabled.
 * 5. Single or multiple authors.
 *
 * @since Twenty Twelve 1.0
 *
 * @param array Existing class values.
 * @return array Filtered class values.
 */
function kobydesign_body_class( $classes ) {
	global $post;

	if ( ! is_page_template( 'page-templates/home.php') AND ! is_front_page() /*AND !is_404()*/ ) {
		$classes[] = 'subpage';
	}

	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	return $classes;
}
add_filter( 'body_class', 'kobydesign_body_class' );

// jQuery
function jquery_custom_script()
{
	if( ! is_admin())
	{
		wp_deregister_script('jquery');
		wp_register_script('jquery', (get_bloginfo('template_url').'/assets/js/vendor/jquery-1.11.0.min.js'), true, '1.11.0');
		wp_enqueue_script('jquery');
	}
}
add_action('wp_enqueue_scripts', 'jquery_custom_script');

/*
*  Change the Options Page menu to 'Theme Options'
*/
 
if( function_exists('acf_set_options_page_title') )
{
    acf_set_options_page_title( 'Opcje strony' );
}
/*
*  Change the Options Page menu to 'Extra'
*/
 
if( function_exists('acf_set_options_page_menu') )
{
    acf_set_options_page_menu( 'Opcje strony' );
}


// Custom image sizes in media library
//add_filter('image_size_names_choose', 'my_image_sizes');

function my_image_sizes($sizes)
{
	$addsizes = array(
		"article-img" => __( "Zdjęcie w artykule")
	);
	$newsizes = array_merge($sizes, $addsizes);
	return $newsizes;
}

// style admina
function admin_head_custom_css()
{
	echo '<style>
	
	</style>';
}
//add_action('admin_head', 'admin_head_custom_css');


// kobydesign_excerpt_length
function kobydesign_excerpt_length( $length ) {
	return 12;
}
add_filter( 'excerpt_length', 'kobydesign_excerpt_length' );

// kobydesign_continue_reading
function kobydesign_continue_reading()
{
	return '<span class="read-more">'. __( 'Zobacz więcej &rarr;', 'kobydesign' ).'</span>';
}

// kobydesign_auto_excerpt_more
function kobydesign_auto_excerpt_more( $more ) {
	return '&hellip;';// . kobydesign_continue_reading();
}
//add_filter( 'excerpt_more', 'kobydesign_auto_excerpt_more' );

// format_month
function format_month($month)
{
	$months = array(
		1 => 'Sty',
		'Lut',
		'Mar',
		'Kwi',
		'Maj',
		'Cze',
		'Lip',
		'Sie',
		'Wrz',
		'Paź',
		'Lis',
		'Gru'
	);
	return (isset($months[$month]))?$months[$month]:'';
}

// CF7 form class
function your_custom_form_class_attr( $class ) {
	$class .= ' form-horizontal';
	return $class;
}
add_filter( 'wpcf7_form_class_attr', 'your_custom_form_class_attr' );


// admin menu rename Posts
function edit_admin_menus() {
    global $menu;
     
    $menu[5][0] = 'Aktualności'; // Change Posts to Recipes
}
//add_action( 'admin_menu', 'edit_admin_menus' );

// Redirect all parent pages to their first child
function my_template_redirect()
{
    global $post;

    $post_type = 'service';

    if ( ! is_object( $post ) )
	{
		return;
	}
        
    // Redirect all parent pages to their first child (determined by the order
    // set in Page Attributes) if one exists
    if ( $post_type == $post->post_type AND 0 == $post->post_parent )
	{
        $child = get_children( array(
            'numberposts' => 1,
            'post_parent' => $post->ID,
            'post_type'   => $post_type,
            'post_status' => 'publish',
            'orderby'     => 'menu_order',
            'order'       => 'ASC'
        ) );

        if ( 1 == count( $child ) ) {
            wp_redirect( get_permalink( current( $child )->ID ), 301 );
            exit;
        }
    }
}

add_action( 'template_redirect', 'my_template_redirect', 0 );




// Automatically Assign Parent Terms When A Child Term is Selected
add_action('save_post', 'assign_parent_terms');

function assign_parent_terms($post_id)
{
    global $post;

    if( ! $post ) { return; }

    $post_type = 'report';
    $post_cat = $post_type.'_cat';

    if($post->post_type != $post_type)
        return $post_id;

    // get all assigned terms   
    $terms = wp_get_post_terms($post_id, $post_cat);
    foreach($terms as $term)
    {
        while($term->parent != 0 && !has_term( $term->parent, $post_cat, $post ))
        {
            // move upward until we get to 0 level terms
            wp_set_post_terms($post_id, array($term->parent), $post_cat, true);
            $term = get_term($term->parent, $post_cat);
        }
    }
}


/*-----------------------------------------------------------------------------
	Custom functions
	-------------------------------------------------------------------------*/

// generatePagination

function generatePagination($paged, $max_num_pages, $base=NULL) 

{

    $big = 999999999; // need an unlikely integer

    if( ! $base )

    {

    	$base = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) );

    }

    echo paginate_links( array(

        'base' => $base,

        'current' => max( 1, $paged ),

        'total' => $max_num_pages,
    ) );

}

//adding colours to palette

function change_mce_options( $init ) {
  $default_colours = '000000,993300,333300,003300,003366,000080,333399,333333,800000,FF6600,808000,008000,008080,0000FF,666699,808080,FF0000,FF9900,99CC00,339966,33CCCC,3366FF,800080,999999,FF00FF,FFCC00,FFFF00,00FF00,00FFFF,00CCFF,993366,C0C0C0,FF99CC,FFCC99,FFFF99,CCFFCC,CCFFFF,99CCFF,CC99FF,FFFFFF';
  $custom_colours = '7b0048';
  $init['theme_advanced_text_colors'] = $default_colours . ',' . $custom_colours;
  $init['theme_advanced_more_colors'] = false;
  return $init;
}
add_filter('tiny_mce_before_init', 'change_mce_options');


//adding responsive class to image

function add_image_responsive_class($content) {
   global $post;
   $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
   $replacement = '<img$1class="$2 img-responsive"$3>';
   $content = preg_replace($pattern, $replacement, $content);
   return $content;
}
add_filter('the_content', 'add_image_responsive_class');
