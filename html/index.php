<?php include 'header.php' ?>

  <!--Content-->

<div class="content" id="ajax-content">

  <div class="container">
        <div class="text-intro" id="site-type">

          <h1>I am Koby. I design.</h1>
          <h1>This is my <span class="typewrite">Portfolio</span></h1>
          <p>"Man can believe the impossible, but man can never believe the improbable." Oscar Wilde</p>

        </div>

      <!--Portfolio grid-->

      <?php $projects = [
        [
          'img'     => 'assets/img/portfolio/5.jpg',
          'link'    => 'single.php',
          'title'   => 'Single',
          'text'    => 'Branding',
        ],
        [
          'img'     => 'assets/img/portfolio/1.jpg',
          'link'    => 'single.php',
          'title'   => 'Single',
          'text'    => 'Branding',
        ],
        [
          'img'     => 'assets/img/portfolio/3.jpg',
          'link'    => 'single.php',
          'title'   => 'Single',
          'text'    => 'Branding',
        ],
        [
          'img'     => 'assets/img/portfolio/5.jpg',
          'link'    => 'single.php',
          'title'   => 'Single',
          'text'    => 'Branding',
        ],
        [
          'img'     => 'assets/img/portfolio/1.jpg',
          'link'    => 'single.php',
          'title'   => 'Single',
          'text'    => 'Branding',
        ],
        [
          'img'     => 'assets/img/portfolio/3.jpg',
          'link'    => 'single.php',
          'title'   => 'Single',
          'text'    => 'Branding',
        ],
      ];
      ?>

      <ul class="portfolio-grid">

      <?php foreach($projects as $project): ?>

          <li class="grid-item" data-jkit="[show:delay=3000;speed=500;animation=fade]">
            <img src="<?php echo $project['img']; ?>">
              <a class="ajax-link" href="<?php echo $project['link']; ?>">  
                <div class="grid-hover">
                  <h1><?php echo $project['title']; ?></h1>
                  <p><?php echo $project['text']; ?></p>
                </div>
              </a>  
          </li>

      <?php endforeach; ?>
     
      </ul>
      
      <div class="clear section"></div>

  </div>

  <!--About me-->

  <div class="container">
      
        <div class="text-intro">

          <h1>About me</h1>
                    
          <div class="one-column">
            <p>Brand designer | Product Designer | Web Junior Designer | Traveler</p>
          
          </div>


          <div class="two-column">

            <p>Hello, my name is Michał Kobierny. I'm graphic designer. I'm freelancer. I invite you to enter my world, world of design.<br/><br/>

            I love branding, meeting with new client, discussing about ideas. I like product design, good looking ads. I learn web design, UX design, mobile design. I code html/php wordpress templates. All the time going forward.
            
            <br/><br/>
            I encourage you to watch my portfolio. Do you like it? Let's do something together!
            
            </p>
          
          </div>          


        </div>
        
        <div class="clear"></div>

  </div>
  
  <!--Contact-->

    <div class="container">

          <div class="text-intro">

          <h1>Contact</h1>
          <p>To contact me please use the contact form below</p>

          <form method="post" action="?" data-jkit="[form:validateonly=true]">
          
            <div class="contact-one">
              <p>
                <label for="miniusername">Name:</label><br />
                <input name="miniusername" id="miniusername" data-jkit="[validate:required=true;min=3;max=10;error=Please enter your username (3-10 characters)]">
              </p>
            </div>  
            
            <div class="contact-two">  
              <p>
                <label for="miniemail">E-mail:</label><br />
                <input name="miniemail" id="miniemail" data-jkit="[validate:required=true;strength=50;error=Please write your email]">
              </p>
            </div>  
            
            <div class="contact-three">  
              <p>
                <label>Message:</label><br />
                <textarea id="message" name="message"></textarea><br/><br/>
                <input class="button-submit" name="send" type="submit" value="SUBMIT" />
              </p>
            </div>            
              
              <p>
              </p>
          </form>
          
        </div>

        <div class="clear"></div>

    </div>

</div>

<?php include 'footer.php' ?>