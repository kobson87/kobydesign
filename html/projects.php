<?php include 'header.php' ?>

<?php include 'elements/top.php' ?>

  <!--Content-->

  <div class="content" id="ajax-content">


        <div class="text-intro">

          <h1>Projects</h1>
          <p>Please check my portfolio. All project is clean and simplicity modern style. You can buy this template.</p>

        </div>


  <!--Portfolio grid-->

  <ul class="portfolio-grid">

    
    <li class="grid-item" data-jkit="[show:delay=3000;speed=500;animation=fade]">
      <img src="assets/img/portfolio/1.jpg">
        <a class="ajax-link" href="single.html">  
          <div class="grid-hover">
            <h1>Single</h1>
            <p>Branding</p>
          </div>
        </a>  
    </li>

    <li class="grid-item" data-jkit="[show:delay=3000;speed=500;animation=fade]">
      <img src="assets/img/portfolio/5.jpg">
        <a class="ajax-link" href="single-fullscreen.html">  
          <div class="grid-hover">
            <h1>Single</h1>
            <p>Branding</p>
          </div>
        </a>      
    </li>    
    
    <li class="grid-item" data-jkit="[show:delay=3000;speed=500;animation=fade]">
      <img src="assets/img/portfolio/2.jpg">
        <a class="ajax-link" href="#">  
          <div class="grid-hover">
            <h1>Single</h1>
            <p>Branding</p>
          </div>
        </a>      
    </li>  
    
    <li class="grid-item" data-jkit="[show:delay=3000;speed=500;animation=fade]">
      <img src="assets/img/portfolio/3.jpg">
        <a class="ajax-link" href="#">  
          <div class="grid-hover">
            <h1>Single</h1>
            <p>Branding</p>
          </div>
        </a>      
    </li>    

    <li class="grid-item" data-jkit="[show:delay=3000;speed=500;animation=fade]">
      <img src="assets/img/portfolio/4.jpg">
        <a class="ajax-link" href="#">  
          <div class="grid-hover">
            <h1>Single</h1>
            <p>Branding</p>
          </div>
        </a>      
    </li>    

    <li class="grid-item" data-jkit="[show:delay=3000;speed=500;animation=fade]">
      <img src="assets/img/portfolio/6.jpg">
        <a class="ajax-link" href="#">  
          <div class="grid-hover">
            <h1>Single</h1>
            <p>Branding</p>
          </div>
        </a>      
    </li>  

  </ul>

  </div>


<?php include 'footer.php' ?>