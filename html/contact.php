<?php include 'header.php' ?>

<?php include 'elements/top.php' ?>

  <!--Content-->

  <div class="content" id="ajax-content">

        <div class="text-intro">

          <h1>Contact</h1>
          <p>To contact us please use the contact form visible. When sending files, please use the following e-mail</p>

          <form method="post" action="?" data-jkit="[form:validateonly=true]">
          
          <div class="contact-one">
            <p>
              <label for="miniusername">Username:</label><br />
              <input name="miniusername" id="miniusername" data-jkit="[validate:required=true;min=3;max=10;error=Please enter your username (3-10 characters)]">
            </p>
          </div>  
          
          <div class="contact-two">  
            <p>
              <label for="miniemail">E-mail:</label><br />
              <input name="miniemail" id="miniemail" data-jkit="[validate:required=true;strength=50;error=Please write your email]">
            </p>
          </div>  
          
          <div class="contact-three">  
            <p>
              <label>Message:</label><br />
              <textarea id="message" name="message"></textarea><br/><br/>
              
              <input class="button-submit" name="send" type="submit" value="SUBMIT" />
            </p>
          </div>            
            
            <p>
            </p>
          </form>
          
          
        </div>
        
	<br/><br/><br/><br/><br/><br/><br/><br/>      
  </div>




  <!--Home Sidebar-->

<div id="ajax-sidebar"></div>



  <!--Footer-->
  <footer>

    <div class="footer-margin">
      <div class="social-footer">
        <a href="https://www.facebook.com/thomsooncom"><i class="fa fa-facebook"></i></a>
        <a href="https://www.behance.net/TomaszMazurczak"><i class="fa fa-behance"></i></a>
      
      </div>     
      <div class="copyright">© Copyright 2015 Thomsoon.com. All Rights Reserved.</div>
      

    
    </div>


  </footer>

<?php include 'footer.php' ?>