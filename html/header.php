<!doctype html>

<html lang="en">
<head>

  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="keywords" content="clean, simplicity, theme, html5, sidebar, masonry">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="author" content="kobydesign.pl">
  <link rel="icon" type="assets/image/png" href="assets/img/kobydesign-sign.png" />

<!--Style-->

  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/reset.css">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="assets/css/style-responsive.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
 
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  
</head>

<body>

  <!--Preloader-->

<div class="preloader" id="preloader">
    <div class="item">
      <div class="spinner">
      </div>
    </div>
</div>

<!--
<div class="opacity-nav">

<div class="menu-index" id="buttons" style="z-index:999999">
        <i class="fa fa-close"></i>
        </div>

    <ul class="menu-fullscreen">
      <li><a class="ajax-link" href="index.php">Home</a></li>
      <li><a class="ajax-link" href="projects.php">Projects</a></li>
      <li><a class="ajax-link" href="about.php">About me</a></li>
      <li><a class="ajax-link" href="contact.php">Contact</a></li>
    </ul>

</div>
-->

<!--Header-->
<header class="boxed" id="header-white">

  <div class="header-margin">

    <a class="logo" href="index.php"><?php include 'assets/img/kobydesign-logotyp.php'; ?></a>
    <!--<div class="menu-index" id="button"><i class="fa fa-bars"></i></div>  -->
    
  </div>

</header>

<div class="clear"></div>
