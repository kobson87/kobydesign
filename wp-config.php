<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kobydesign2016');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'U:A>byhL yr@XjzuQYJL+V(nrb).#%w=J~|f)w5K8?legW}~lKk[zax@6T?~qc$s');
define('SECURE_AUTH_KEY',  '5jW4xCZG@*B(??Ehi56V48Bt]8z:/RkEI+RlewwT!UXP%- `D/P)/S|stNDsl?Q1');
define('LOGGED_IN_KEY',    'e.v%fhXBL)Eoyd)36 DbWeV=E+Pp-(CW.a1S9a&@hO3%q2mHYxhq$nrpSh[I%|M~');
define('NONCE_KEY',        '..av0i:4CNUhbN|/]  *3Wxc0}jdMNe8Mfh~*F8EeR.WCHc!<xw$4GH~m52ki]!>');
define('AUTH_SALT',        't5&ay?xvcvN+Rf $}Tz=Biho%;eGiYIk&1DpqFEM`>Zw!-+J?Ot;+~%JpP~jLIY]');
define('SECURE_AUTH_SALT', '0YBM%h5maDd5WHg}$/at,B%3XM1GVJN~/[k8u,QE10&T)H}IkkkQEdEWiU-lO9]}');
define('LOGGED_IN_SALT',   'ATbsVM76=o05O`x-;i|%z]ZERO|W>r1nD,crzYVttuxKn)Eg*l@oU+B@:_|.rf_<');
define('NONCE_SALT',       '?Fu47H$[.#|AeI9s4tlK1.6<Zt8V4d]^7~&XFav(y};mHo50SjF33`0A-B3@b i|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
